<html>
    <head>
        <title>php1.php</title>
    </head>
    <body>
        <?php
        function CalcularPotencia($base, $exponente)
        {
            $potencia = $base;
            
            for ($i = 1; $i < $exponente; $i++)
            {
                $potencia *= $base;
            }
            
            return $potencia;
        }
        
        echo CalcularPotencia(2, 3);
        ?>
    </body>
</html>
