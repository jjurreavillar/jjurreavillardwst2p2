<?php
define("LIMITE", 100);

function CalcularPrimos()
{
    $primos = array();
    
    for ($i = 2; $i <= LIMITE; $i++)
    {
        $continuar = false;
        
        for ($x = 2; $x < $i; $x++)
        {
            if ($i % $x == 0)
            {
                continue 2;
                /* Alternativamente
                $continuar = true;
                break;
                */
            }
        }
        /*
        if ($continuar)
        {
            continue;
        }
        */
        array_push($primos, $i);
    }
    
    return $primos;
}
?>

<html>
    <head>
        <title>php3.php</title>
    </head>
    <body>
        <?php
            $primos = CalcularPrimos();
            
            foreach($primos as $primo)
            {
                echo $primo . "<br>";
            }
        ?>
    </body>
</html>
